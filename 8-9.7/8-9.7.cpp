#include <stdio.h>
#include <math.h>
int main()
{
	int v;
	double res;
	v = 0;
	for (double x = 2; x < 4; x += 0.1)
	{
		res = exp(pow(x, 2)) / 2;
		v += 1;
		printf("\n Answer %d = %lf", v, res);
	}
}

